//
//  demoiftttApp.swift
//  demoifttt
//
//  Created by Emilio Brambilla on 25/05/22.
//

import SwiftUI

@main
struct demoiftttApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
