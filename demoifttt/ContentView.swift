//
//  ContentView.swift
//  demoifttt
//
//  Created by Emilio Brambilla on 25/05/22.
//

import SwiftUI

import WebKit

//tado: Connect2022!

struct ContentView: View {
    let urlString = "https://ifttt.com/applets/tSHR2KUk-turn_of_smart_lights"
    
    var body: some View {
        
        VStack{
            Image("connect-reply")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(height: 80)
                
            
            WebView(url: URL(string: urlString)!)
        }
        .background(Color.white)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


struct WebView: UIViewRepresentable {
 
    var url: URL
 
    func makeUIView(context: Context) -> WKWebView {
        

        return WKWebView()
    }
 
    func updateUIView(_ webView: WKWebView, context: Context) {
        let request = URLRequest(url: url)
        webView.load(request)
        webView.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

    }
}
