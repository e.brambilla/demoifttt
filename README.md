# DemoIFTTT



## Getting started

1. Download Xcode

2. clone repository on macbook 

3. open repository and locate DemoIFTTT.xcodeproj

4. double click on it and open it with xcode

5. click on project and select personal team (connect with ANY Apple ID, it does not matter)

   ![Alt text](screenshots/1.png?raw=true "Optional Title")

6. connect lightning cable to macbook and connect iphone

7. Select architecture
   ![Alt text](screenshots/2.png?raw=true "Optional Title")

8. choose the connected iphone 
   ![Alt text](screenshots/3.png?raw=true "Optional Title")

9. click run
   ![Alt text](screenshots/4.png?raw=true "Optional Title")
10. Should work, for issues contact me
